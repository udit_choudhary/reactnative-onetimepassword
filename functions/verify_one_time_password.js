const admin = require('firebase-admin');



module.exports = (req,res) => {

if(!req.body.phone || !req.body.code){
  return res.status(422).send({error: "Inavlid input."})
}

const phone = String(req.body.phone).replace(/[^\d]/g,'');
const code = parseInt(req.body.code);

admin.auth().getUser(phone)
.then(userRecord => {

  const ref = admin.database().ref('users/'+phone);
  ref.on('value', snapshot => {
    ref.off(); //to stop listening for changes
    const user = snapshot.val();
    if(user.code !== code || !user.codeValid){
      return res.status(422).send(
        {
          error: "Code not valid"
        });
    }
      ref.update({codeValid: false});

      admin.auth().createCustomToken(phone,{premiumAccount: true})
      .then(token => res.send({ token: token }))
      .catch( (err) => {return res.status(422).send({error:err});})
   });

})
.catch((err) => {
  return res.status(422).send({error: err});
})

}
