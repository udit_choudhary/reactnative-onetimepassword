const functions = require('firebase-functions');
const createUser = require('./create_user');
const admin = require('firebase-admin');
const requestOneTimePassword = require('./request_one_time_password');
const verifyOneTimePassword = require('./verify_one_time_password');

const serviceAccount = require('./serviceAccountKey.json')
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://one-time-password-b745a.firebaseio.com"
});

//insisting of initializing app below, we can do it in above stated way. But for that, we need to get the serviceaccount credenitals and
//store in a json file. This way is not secure as anyone with that json file can access your firebase account.
//But for AWS we cant use the followin, we have to use the above solution

//Below default doesnt work with createCustomToken
// admin.initializeApp(functions.config().firebase);

exports.createUser = functions.https.onRequest(createUser);
exports.requestOneTimePassword = functions.https.onRequest(requestOneTimePassword);
exports.verifyOneTimePassword = functions.https.onRequest(verifyOneTimePassword);
