const admin = require('firebase-admin');


module.exports = (req,res) => {


//verify if user has entered a number else return error 422 with msg wrong input
if(!req.body.phone){
  return res.status(422).send({error: 'Invalid phone number'});
}
//replace all the non alpha chars from the phone
const phone = String(req.body.phone).replace(/[^\d]/g,'');


//create a new user serviceAccount
admin.auth().createUser({uid: phone})
.then(user => res.send(user))
.catch(err => res.status(422).send({error: err}));

//send response to users request


}
